#![allow(dead_code)]
#![recursion_limit="256"]
extern crate proc_macro;
use proc_macro::TokenStream;
use proc_macro2::TokenStream as TStream;
use syn::{Ident, Item, TraitItem, parse_macro_input, ItemTrait, ItemStruct, punctuated::Punctuated, FnArg, token::Comma, Pat};
use quote::quote;
use std::iter::FromIterator;

#[proc_macro_attribute]
pub fn interface(attr : TokenStream, item : TokenStream) -> TokenStream {
    let cloned = item.clone();
    let parsed : Item = parse_macro_input!(item as Item);

    let args : Punctuated<syn::Ident, Comma> = Punctuated::from_iter(
        attr.into_iter()
            .step_by(2)
            .map(|token| syn::parse::<syn::Ident>(token.into()).unwrap() )
    );
    match parsed {
        Item::Trait(trait_) => trait_interface(&trait_),
        Item::Struct(struct_) => struct_interface(args, &struct_),
        _ => cloned
    }
}

// generates a vtable for a trait
fn trait_interface (trait_ : &ItemTrait) -> TokenStream {
    let mut extern_methods = quote!{};
    let mut struct_members = quote!{};
    let mut initializers   = quote!{};
    let mut impl_methods   = quote!{};
    let trait_ident  = &trait_.ident;

    for item in &trait_.items {
        match item {
            TraitItem::Method(method) => {
                let ident = &method.sig.ident;
                let ret = &method.sig.decl.output;
                let orig_args = &method.sig.decl.inputs;
                let (args, method, keep_first) =
                    convert_self(&method.sig.decl.inputs);
                let call_args = arg_idents(&args, keep_first);
                let orig_call_args = arg_idents(&orig_args,false);

                let ptr_cast = if let Some(FnArg::SelfRef(self_)) = orig_args.first().map(|a| a.into_value()) {
                    if self_.mutability.is_some() {
                        quote!()
                    } else {
                        quote!(as *const c_void)
                    }
                } else {
                    panic!("Only methods taking self are supported.")
                };

                extern_methods = extern_methods.append (quote! {
                    pub extern fn #ident <T: #trait_ident> (#args) # ret {
                        unsafe { #method #ident (#call_args) }
                    }
                });
                impl_methods = impl_methods.append (quote! {
                    fn #ident (#orig_args) # ret {
                        (self.vtable.#ident)(self.reference #ptr_cast, #orig_call_args)
                    }
                });
                struct_members = struct_members.append (quote!{
                    #ident : extern fn (#args) #ret,
                });
                initializers = initializers.append (quote!{
                    #ident : #ident::<T>,
                });
            }
            _ => ()
        }
    }

    let internal_methods = quote! {
        pub extern fn clone_ref <T: #trait_ident + Clone> (src: *const c_void) -> *mut c_void {
            let src = src as *const T;
            let out = unsafe {Box::into_raw(Box::new((*src).clone())) };
            out as *mut c_void
        }
    };

    let mod_= trait_ident.clone().prepend("ability_");
    let vtable = trait_ident.clone().append("VTable");
    let obj = trait_ident.clone().append("Obj");
    let trait_str = &trait_ident.to_string();
    let expanded = quote! {
    #trait_

    pub use #mod_::#obj;

    #[allow(non_snake_case)]
    #[allow(dead_code)]
    pub mod #mod_ {
        use super::*;
        use std::{
            ffi::c_void,
            marker::PhantomData,
            error::Error,
        };

        mod __internal__ {
            #internal_methods
        }

        #extern_methods

        #[repr(C)]
        struct #vtable {
            #struct_members
        }

        impl #vtable {
            pub fn new <T: #trait_ident>() -> Self {
                Self {
                    #initializers
                }
            }
        }

        #[repr(C)]
        pub struct #obj <'a> {
            _marker: PhantomData<&'a ()>,
            reference: *mut c_void,
            vtable: #vtable
        }

        impl <'a> #obj <'a> {
            pub fn load(lib: &'a abi_trait::libloading::Library) -> Result<#obj <'a>, Box<dyn Error>> {
                unsafe {abi_trait::load::<#obj <'a>>(lib,#trait_str)}
            }
            pub fn __new__ <T: #trait_ident + Default> () -> #obj <'a> {
                let _reference = Box::into_raw(Box::new(T::default()));
                #obj {
                    _marker: PhantomData,
                    reference: _reference as *mut c_void,
                    vtable: #vtable ::new::<T>(),
                    clone_ref: __internal__::clone_ref::<T>,
                }
            }
        }

        impl <'a> #trait_ident for #obj <'a> {
            #impl_methods
        }
        impl <'a> Clone for #obj <'a> {
            fn clone(&self) -> Self {
                Self {
                    _marker: PhantomData,
                    reference: (self.clone_ref)(self.reference as *const c_void),
                    vtable: self.vtable.clone(),
                    clone_ref: self.clone_ref.clone(),
                }
            }
        }
    }
    };
    TokenStream::from (expanded)
}

// maps &self/&mut self args to *const c_void/*mut c_void. The additional token stream result is
// used to wrap the method calls.
fn convert_self (args : &Punctuated<FnArg, Comma>) -> (Punctuated<FnArg, Comma>, TStream, bool) {
    let mut lookup = quote!(T::);
    let mut keep_first = true;
    let it = args
        .iter()
        .map(|arg|{
            match arg {
                FnArg::SelfRef(self_ref) => {
                    keep_first = false;
                    let sig =
                        if self_ref.mutability.is_some() {
                            lookup = quote!( (* (self_ as *mut T) ) . );
                            quote!(*mut std::os::raw::c_void)
                        }
                        else {
                            lookup = quote!( (* (self_ as *const T)) . );
                            quote!(*const std::os::raw::c_void)
                        };

                    FnArg::Captured(syn::ArgCaptured {
                        pat         : syn::parse(quote!(self_).into()).unwrap(),
                        colon_token : syn::parse(quote!(:).into()).unwrap(),
                        ty          : syn::parse(sig.into()).unwrap()
                    })
                },
                _ => arg.clone()
            }
        });
    (Punctuated::from_iter(it), lookup, keep_first)
}

fn arg_idents (args : &Punctuated<FnArg, Comma>, keep_first : bool) -> Punctuated <Pat, Comma> {
    let it = args
        .iter()
        .skip(if keep_first {0} else {1})
        .map(|arg|
            match arg {
                FnArg::Captured(captured) => captured.pat.clone(),
                _ => panic!("no inferred type, &self or Self arguments supported")
            })
        .into_iter();

    Punctuated::from_iter(it)
}

fn struct_interface(traits : Punctuated<Ident, Comma>, struct_ : &ItemStruct) -> TokenStream {
    let mut inner = quote!();

    let ident = &struct_.ident;

    for trait_ in &traits {
        let obj = trait_.clone().append("Obj");
        let literal = format!(r#"{}"#, trait_);
        let literal = &literal;

        inner = inner.append (quote!{
            #literal => unsafe {
                std::panic::catch_unwind(|| {
                    *(obj as *mut #obj) = #obj::__new__::<#ident>();
                }).map_err(|e| std::panic::resume_unwind(e));
            },
        });
    }

    let expanded = quote! {
        #struct_

        #[no_mangle]
        pub extern fn get_obj (
            interface : *const std::os::raw::c_char,
            obj    : *mut   std::os::raw::c_void
        ) {
            use std::{os::raw::c_void, ffi::CStr};
            let interface = unsafe {
                CStr::from_ptr(interface)
                    .to_str()
                    .expect("invalid identifier")
            };

            match interface {
                #inner
                _ => ()
            }
        }
    };
    TokenStream::from(expanded)
}

trait Append <T> {
    fn append(self, suffix : T) -> Self;
    fn prepend(self, prefix : T) -> Self;
}

impl<'a> Append<&'a str> for syn::Ident {
    fn append (self, suffix : &str) -> Self {
        let s = format!("{}{}", self, suffix);
        syn::Ident::new(&s, self.span())
    }

    fn prepend (self, prefix : &str) -> Self {
        let s = format!("{}{}", prefix, self);
        syn::Ident::new(&s, self.span())
    }
}

impl Append<TStream> for TStream {
    fn append(mut self, suffix : TStream) -> Self {
        self.extend(suffix);
        self
    }

    fn prepend(self, mut prefix : TStream) -> Self {
        prefix.extend (self);
        prefix
    }
}